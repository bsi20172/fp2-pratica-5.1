
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {
    public static void main(String[] args) throws MatrizInvalidaException, ProdMatrizesIncompativeisException, SomaMatrizesIncompativeisException {
        try{
            Matriz m1 = new Matriz(1,2);
            Matriz m2 = new Matriz (3,2);
            System.out.println(m1.prod(m2));
            System.out.println(m1.soma(m2));
        }
        catch(MatrizInvalidaException | SomaMatrizesIncompativeisException | ProdMatrizesIncompativeisException me){
            System.out.println(me);
        }
    }
}
