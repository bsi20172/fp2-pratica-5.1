package utfpr.ct.dainf.if62c.pratica;

import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;

/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    private int m;
    private int n;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int m, int n) throws MatrizInvalidaException {
        if(m <= 0 || n <= 0)
                throw new MatrizInvalidaException(m, n);
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException
     */
    public Matriz soma(Matriz m) throws SomaMatrizesIncompativeisException {
        if(this.mat[0].length != m.mat[0].length || this.mat.length != m.mat.length)
            throw new SomaMatrizesIncompativeisException(this, m);
        
        for(int i = 0; i < mat.length; i++)
            for(int j = 0; j < mat[0].length; j++)
                m.mat[i][j]  += mat[i][j];
        return m;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz prod(Matriz m) throws ProdMatrizesIncompativeisException, MatrizInvalidaException {
        if(this.mat[0].length != m.mat.length)
            throw new ProdMatrizesIncompativeisException(this, m);
        Matriz p = new Matriz(mat.length, m.mat[0].length);
                
        for(int i = 0; i < mat.length; i++)
            for(int j = 0; j < m.mat[0].length; j++)
                for(int k = 0; k < mat[0].length; k++)
                    p.mat[i][j] += mat[i][k] * m.mat[k][j];
        
        return p;
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}
